function listeners(){
    /*loginPage*/
    document.querySelector('#loginBtn').addEventListener('click', viewLogin);
    document.querySelector('#registerBtn').addEventListener('click', viewRegister);

    /*pageOne*/
    document.querySelector('#sectionOneBtn').addEventListener('click', viewSectionOne);
    document.querySelector("#logoutBtn").addEventListener('click', viewInit);
}

window.onload = function(){
    listeners();
}

let viewLogin = ()=>{
    document.querySelector("#loginForm").classList.remove('hidden');
    document.querySelector("#registerForm").classList.add('hidden');
}


let viewRegister = ()=>{
    document.querySelector("#loginForm").classList.add('hidden');
    document.querySelector("#registerForm").classList.remove('hidden');
}
let viewSectionOne = ()=>{
    document.querySelector("#loginPage").classList.add('hidden');
    document.querySelector("#sectionOne").classList.remove('hidden');
}
let viewInit = ()=>{
    document.querySelector("#loginPage").classList.remove('hidden');
    document.querySelector("#sectionOne").classList.add('hidden');
}